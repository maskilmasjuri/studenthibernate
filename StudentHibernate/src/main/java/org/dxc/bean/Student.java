package org.dxc.bean;

import javax.persistence.*;

@Entity
@Table(name = "STUDENT")
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "sname")
	private String sname;
	
	@Column(name = "saddr")
	private String saddr;
	
	public Student() {};
	
	public Student(String sname, String saddr) {
		this.sname = sname;
		this.saddr = saddr;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSaddr() {
		return saddr;
	}
	public void setSaddr(String saddr) {
		this.saddr = saddr;
	}
	
	

}
