package org.dxc.controller;

import java.io.*;
import java.util.*;

import org.dxc.bean.Student;
import org.dxc.factorydesign.ServiceFactory;
import org.dxc.service.StudentService;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) throws IOException {

		Student std = new Student();
		int sid;
		String sname;
		String saddr;
		StudentService service = ServiceFactory.getServiceObject();
		int option = -1;

		BufferedReader b = new BufferedReader(new InputStreamReader(System.in));

		

		do {
			System.out.println(" ");
			System.out.println("Select an option:");
			System.out.println("1) Add Student Record");
			System.out.println("2) View All Student Records");
			System.out.println("3) View Student Record By ID");
			System.out.println("4) Update Student Record");
			System.out.println("5) Delete Student Record");
			System.out.println("0) Exit");
			
			option = Integer.parseInt(b.readLine());
			switch (option) {
			case 1:
				System.out.println("Name?");
				sname = b.readLine();

				System.out.println("Address?");
				saddr = b.readLine();
				std.setSname(sname);
				std.setSaddr(saddr);

				service.insert(std);

				break;
			case 2:
				service.getAllStudents();
				break;

			case 3:
				System.out.println("ID?");
				sid = Integer.parseInt(b.readLine());
				std = service.getByID(sid);
				System.out.print("Student ID: " + sid);
				System.out.print("  Name: " + std.getSname());
				System.out.println("  Last Name: " + std.getSaddr());
				break;

			case 4:
				System.out.println("ID?");
				sid = Integer.parseInt(b.readLine());
				std = service.getByID(sid);
				sname = std.getSname();
				saddr = std.getSaddr();
				System.out.println("Student Name " + sname + " has old address: " + saddr);
				System.out.println("New address?");
				saddr = b.readLine();
				std.setSaddr(saddr);
				service.updateRecord(std);

				System.out.print("Student ID: " + sid);
				System.out.print("  Name: " + std.getSname());
				System.out.println("  Last Name: " + std.getSaddr());

				break;

			case 5:
				System.out.println("ID to delete?");
				sid = Integer.parseInt(b.readLine());
				service.deleteRecord(sid);
				System.out.println("Record deleted");
				break;

			default:
				break;
			}

		} while (option != 0);

	}
}
