package org.dxc.service;

import org.dxc.bean.Student;

public interface StudentService {
	Integer insert(Student s);
	void getAllStudents();
	Student getByID(int id);
	void updateRecord(Student s);
	void deleteRecord(int id);
	
}
