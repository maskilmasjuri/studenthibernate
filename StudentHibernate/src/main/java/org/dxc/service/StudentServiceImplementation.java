package org.dxc.service;

import java.util.Iterator;
import java.util.List;

import org.dxc.bean.Student;
import org.dxc.factorydesign.HibernateFactory;
import org.hibernate.*;

public class StudentServiceImplementation implements StudentService {

	public Integer insert(Student s) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Transaction tx = null;
		Integer studentId = null;

		try {
			tx = session.beginTransaction();
			studentId = (Integer) session.save(s);
			tx.commit();
			System.out.println("Student record created succesfully...");
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return studentId;
	}

	public void getAllStudents() {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List students = session.createQuery("FROM Student").list();
			for (Iterator iterator = students.iterator(); iterator.hasNext();) {
				Student student = (Student) iterator.next();
				System.out.print("Name: " + student.getSname());
				System.out.println("  Last Name: " + student.getSaddr());
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Student getByID(int id) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Student student = null;
		Transaction tx = null;

		try {
			tx = session.beginTransaction();

			student = (Student) session.get(Student.class, id);
//			System.out.print("Student ID: " + id);
//			System.out.print("  Name: " + student.getSname());
//			System.out.println("  Last Name: " + student.getSaddr());

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return student;
	}

	public void updateRecord(Student s) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Student student = (Student) session.get(Student.class, s.getId());
			student.setSaddr(s.getSaddr());
			session.update(student);
			System.out.println("Student record updated");
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public void deleteRecord(int id) {
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			Student student = (Student) session.get(Student.class, id);
			session.delete(student);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

}
