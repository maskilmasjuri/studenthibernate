package org.dxc.factorydesign;

import org.dxc.service.StudentService;
import org.dxc.service.StudentServiceImplementation;

public class ServiceFactory {
	private static final StudentService service;
	
	static {
		service = new StudentServiceImplementation();
	}
	
	public static StudentService getServiceObject() {
		return service;
	}
}
