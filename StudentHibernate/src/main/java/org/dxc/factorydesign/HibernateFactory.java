package org.dxc.factorydesign;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {
	private static final SessionFactory factory;
	
	static {
		factory = new Configuration().configure().buildSessionFactory();
	}
	
	public static SessionFactory getFactoryObject() {
		return factory;
	}
}
